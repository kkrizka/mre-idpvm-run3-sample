# Installation
The following commands will build the workspace after making a recursive clone of this workspace:

```shell
asetup 22.0.63,Athena
cmake -Ssource -Bbuild -DATLAS_PACKAGE_FILTER_FILE=source/package_filters.txt
source build/${Athena_PLATFORM}/setup.sh
cmake --build build/
```

# Setup
Run the following at the start of every session.

```sh
source reconstruction/setup.sh [build]
```

The setup script setups the build directory and restores the corresponding Athena version.

The following convenient environmental variables are set:
- `MYBUILD`: Path to the build directory
- `MYWORKSPACE`: Path to the directory containing `setup.sh`

The optional `[build]` argument can be used to specify a non-default location fo the build directory. The default is `${MYWORKSPACE}/build`.
