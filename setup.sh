if [ ${#} != 0 ] && [ ${#} != 1 ]; then
    echo "usage: ${0} [build]"
    return 1
fi

#
# Workspace is where I am
export MYWORKSPACE=$(realpath $(dirname ${BASH_SOURCE[0]}))

#
# Check for build directory
MYBUILD=${MYWORKSPACE}/build
if [ ${#} == 1 ]; then
    MYBUILD=${1}
fi

if [ ! -d ${MYBUILD} ]; then
    echo "Build directory ${MYBUILD} does not exist!"
    return 1
fi

# Convert to absolute path
export MYBUILD=$(realpath ${MYBUILD})

#
# Main software
cd ${MYBUILD}
source $AtlasSetup/scripts/asetup.sh --restore
source x86_64-*/setup.sh
cd -
